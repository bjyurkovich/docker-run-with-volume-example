FROM ubuntu:20.04

# Update stuff
RUN DEBIAN_FRONTEND=noninteractive apt-get update --fix-missing

# install python3.8
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install software-properties-common
RUN DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:deadsnakes/ppa -y
RUN DEBIAN_FRONTEND=noninteractive apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install python3.8 -y
RUN DEBIAN_FRONTEND=noninteractive apt install -y python3-pip
RUN DEBIAN_FRONTEND=noninteractive pip3 install virtualenv
RUN echo 'alias python=python3.8' >> ~/.bashrc

# set up where your processing files run
RUN mkdir /workspace
COPY ./main.py /workspace

# Run your entry script
CMD ["python3", "/workspace/main.py"]

