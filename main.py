import os

option = os.getenv("RUN_OPTION")

if option == "production":
    print("I am running in production mode")
else:
    print("I am not running in production mode")

with open('/data_folder_in_container/data1.txt', 'r') as f:
    data1 = f.read()

print(data1)


with open('/data_folder_in_container/data2.txt', 'r') as f:
    data2 = f.read()

print(data2)