# Example Docker Container Running without VS Code

Clone this repo, then:

Install `docker-compose` on the server

```
 sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
 sudo chmod +x /usr/local/bin/docker-compose
 sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
 docker-compose --version
```

and build the image:

```
docker-compose build
```

Once the image is built (will take a while), run it:

```
docker-compose up
```

The output should say:

```
Recreating example_data_processor ... done
Attaching to example_data_processor
example_data_processor    | I am running in production mode
example_data_processor    | This is data in data1.txt.
example_data_processor    | This is data in data2.txt
example_data_processor exited with code 0
```

### What it does
There is currently a single `main.py` file that imports two `txt` files from a mounted volume (`data_folder_on_host`) that is mounted to a folder inside the container called `data_folder_in_container`.  The line in the docker-compose file (yml file) that makes this happen is: 

```
volumes:
      - "./data_folder_on_host:/data_folder_in_container"
```

The entrypoint in in the `Dockerfile` runs the python script `main.py`:

```
CMD ["python3", "/workspace/main.py"]
```

Once the script is complete (loading the data) it exits and halts the container.

## Equivalent Docker Commands (if you don't  want to use `docker-compose`)
Assuming you used the build command to build the container:

```
docker build -t docker-run-example_example_data_processor .
```

The equivalent docker command to run this container is 

```
docker run -v $(pwd)/data_folder_on_host:/data_folder_in_container -e RUN_OPTION=production docker-run-example_example_data_processor
```

